library("GenomicRanges")
library("BSgenome.Sscrofa.UCSC.susScr11")
library("Repitools")
library("stringr")


windows <- readRDS("windows_geneome.RDS")
ssLengths <- seqlengths(Sscrofa)[paste0("chr", c(1:18, "X", "Y"))]
names(ssLengths) <- str_replace(names(ssLengths), "chr", "")
seqlengths(windows()) <- ssLengths


tem_file <- list.files(".", pattern = "^Tem*")

lapply(tem_file, function(x) {
    
    tem <- readRDS(x)
    windows$score <- tem
    windows$score[is.na(windows$score)] <- 0
    export(windows, str_replace(x, ".rds", ".bigwig"), format = "bigWig")
    message(paste0(x, "file converted to bigwig."))
    return()
})