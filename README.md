# ROSEpigs analysis
Script and data used for the analysis of pig duodenum transcritomes and DNA methylation profiles.

It refers to the manuscript:
*Stronger transcriptomic response to feed intake in the duodenum of pig with high feed efficiency from a divergent selection experiment*

## *fastq* files
Demultiplexed fastq files are available at the European Nucleotide Archive, under the accessions PRJEB46060 / ERP130249.

## Transcriptomic analysis
### Reads processing 
RNAseq reads were processed using [nf-core/rnaseq](https://github.com/nf-core/rnaseq) V3.0 on the [GenoToul](http://bioinfo.genotoul.fr/) compute facility using:  

[nfcore_rnaseq.sh](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/nfcore_rnaseq.sh)  

Results were locally stored under the path: 
`/work2/genphyse/genepi/data/porc/rnaseq/ROSEpigs_processed2/results`

### Visualistations and differential gene expression analysis
Transcriptomic analyses were then executed interactively using the R script [1_diffAnalysis.R](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/01_diffAnalysis.R).

### Usefull processed table

A normalise gene expression (`lengthScaledTPM` from `{tximport}`) table is available at: 
[processed_files/gene_expressions_lengthScaledTPM.tsv](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/processed_files/gene_expressions_lengthScaledTPM.tsv)

Corresponding sample metadata is available at:
[processed_files/samples_metadata.tsv](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/processed_files/samples_metadata.tsv)
Use the `id` column to match the tables.

Results from the three differential gene expression analysis are available at:
[processed_files/differentially_expressed_genes.tsv](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/processed_files/differentially_expressed_genes.tsv)

## DNA methylation analysis
**Work in progress**
### Reads processing 
MeDPSeq reads were processed using [nf-core/chipseq](https://github.com/nf-core/chipseq) V1.2.1 on the [GenoToul](http://bioinfo.genotoul.fr/) compute facility using:  

[nfcore_chipseq.sh](https://forgemia.inra.fr/genepi/analyses/rosepigs/-/blob/master/nfcore_chipseq.sh)  

Results were locally stored under the path: 
`/work2/genphyse/genepi/data/porc/medpseq/ROSEpigs_processed2/results/`

## R session information

```r
> sessionInfo()
R version 4.2.1 (2022-06-23)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 22.04.1 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.10.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.10.0

locale:
 [1] LC_CTYPE=fr_FR.UTF-8       LC_NUMERIC=C               LC_TIME=fr_FR.UTF-8        LC_COLLATE=fr_FR.UTF-8     LC_MONETARY=fr_FR.UTF-8   
 [6] LC_MESSAGES=fr_FR.UTF-8    LC_PAPER=fr_FR.UTF-8       LC_NAME=C                  LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=fr_FR.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] grid      stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] enrichplot_1.16.1     biomaRt_2.52.0        org.Ss.eg.db_3.15.0   AnnotationDbi_1.58.0  Biobase_2.56.0        clusterProfiler_4.4.4
 [7] httr_1.4.3            jsonlite_1.8.0        seriation_1.3.5       dendextend_1.16.0     svglite_2.1.0         UpSetR_1.4.0         
[13] purrr_0.3.4           circlize_0.4.15       ComplexHeatmap_2.12.0 patchwork_1.1.1       ggplot2_3.3.6         edgeR_3.38.1         
[19] limma_3.52.2          stringr_1.4.0         tximport_1.24.0       rtracklayer_1.56.1    GenomicRanges_1.48.0  GenomeInfoDb_1.32.2  
[25] IRanges_2.30.0        S4Vectors_0.34.0      BiocGenerics_0.42.0   data.table_1.14.2    

loaded via a namespace (and not attached):
  [1] shadowtext_0.1.2            fastmatch_1.1-3             BiocFileCache_2.4.0         systemfonts_1.0.4           plyr_1.8.7                 
  [6] igraph_1.3.3                lazyeval_0.2.2              splines_4.2.1               BiocParallel_1.30.3         digest_0.6.29              
 [11] foreach_1.5.2               yulab.utils_0.0.5           GOSemSim_2.22.0             viridis_0.6.2               GO.db_3.15.0               
 [16] fansi_1.0.3                 magrittr_2.0.3              memoise_2.0.1               cluster_2.1.3               doParallel_1.0.17          
 [21] Biostrings_2.64.0           graphlayouts_0.8.0          matrixStats_0.62.0          prettyunits_1.1.1           colorspace_2.0-3           
 [26] rappdirs_0.3.3              blob_1.2.3                  ggrepel_0.9.1               dplyr_1.0.9                 crayon_1.5.1               
 [31] RCurl_1.98-1.7              scatterpie_0.1.7            iterators_1.0.14            ape_5.6-2                   glue_1.6.2                 
 [36] polyclip_1.10-0             registry_0.5-1              gtable_0.3.0                zlibbioc_1.42.0             XVector_0.36.0             
 [41] GetoptLong_1.0.5            DelayedArray_0.22.0         shape_1.4.6                 scales_1.2.0                DOSE_3.22.0                
 [46] DBI_1.1.3                   Rcpp_1.0.9                  progress_1.2.2              viridisLite_0.4.0           clue_0.3-61                
 [51] gridGraphics_0.5-1          tidytree_0.3.9              bit_4.0.4                   fgsea_1.22.0                RColorBrewer_1.1-3         
 [56] ellipsis_0.3.2              pkgconfig_2.0.3             XML_3.99-0.10               farver_2.1.1                dbplyr_2.2.1               
 [61] locfit_1.5-9.6              utf8_1.2.2                  ggplotify_0.1.0             tidyselect_1.1.2            rlang_1.0.4                
 [66] reshape2_1.4.4              munsell_0.5.0               tools_4.2.1                 cachem_1.0.6                downloader_0.4             
 [71] cli_3.3.0                   generics_0.1.2              RSQLite_2.2.14              fastmap_1.1.0               yaml_2.3.5                 
 [76] ggtree_3.4.0                bit64_4.0.5                 tidygraph_1.2.1             KEGGREST_1.36.2             ggraph_2.0.5               
 [81] nlme_3.1-157                aplot_0.1.6                 xml2_1.3.3                  DO.db_2.9                   compiler_4.2.1             
 [86] rstudioapi_0.13             filelock_1.0.2              curl_4.3.2                  png_0.1-7                   treeio_1.20.0              
 [91] tibble_3.1.7                tweenr_1.0.2                stringi_1.7.8               lattice_0.20-45             Matrix_1.4-1               
 [96] vctrs_0.4.1                 pillar_1.7.0                lifecycle_1.0.1             GlobalOptions_0.1.2         bitops_1.0-7               
[101] qvalue_2.28.0               R6_2.5.1                    BiocIO_1.6.0                TSP_1.2-0                   gridExtra_2.3              
[106] codetools_0.2-18            MASS_7.3-58                 assertthat_0.2.1            SummarizedExperiment_1.26.1 rjson_0.2.21               
[111] withr_2.5.0                 GenomicAlignments_1.32.0    Rsamtools_2.12.0            GenomeInfoDbData_1.2.8      hms_1.1.1                  
[116] parallel_4.2.1              ggfun_0.0.6                 tidyr_1.2.0                 MatrixGenerics_1.8.1        ggforce_0.3.3              
[121] restfulr_0.0.15            
```
