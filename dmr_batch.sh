#!/bin/bash
#SBATCH -J dmrseq
#SBATCH -p unlimitq
#SBATCH --mem=92G
#SBATCH --cpus=4
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH -o output.out
#SBATCH -e error.out


Rscript /work2/genphyse/genepi/guillaume/rosepigs/dmr_analysis.R
